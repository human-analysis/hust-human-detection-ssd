import cv2
import numpy as np
import torch

from src.config.defaults import _C as cfg
from src.modeling.backbone import MobileNetV2

if __name__ == "__main__":
    cfg.merge_from_file(
        "configs/mobilenet_v2_ssd320.yaml")
    image_file = "datasets/custom_data/train/cuhk_camera_train/img/s12472.jpg"
    ann_file = "datasets/custom_data/train/cuhk_camera_train/ann/s12472.txt"

    image = cv2.imread(image_file)
    image = cv2.resize(image, (320, 320))
    image = image.reshape(1, 3, 320, 320)
    images = np.concatenate((image, image), axis=0)
    images = torch.FloatTensor(images)

    model = MobileNetV2(config_params=cfg.MODEL.BACKBONE.CFG_PARAMS.EXTRAS_PARAMS)
    feats = model(torch.tensor(images))
    print(feats[0])
