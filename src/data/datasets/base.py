import torch.utils.data

from src.structures.container import Container


class Base(torch.utils.data.Dataset):
    def __init__(self, transform, target_transform, keep_difficult=None):
        self.transform = transform
        self.target_transform = target_transform
        self.keep_difficult = keep_difficult
        self.ids = []

    def __getitem__(self, index):
        image_id = self.ids[index]
        boxes, labels, is_difficult = self._get_annotation(image_id)
        if not self.keep_difficult:
            boxes = boxes[is_difficult == 0]
            labels = labels[is_difficult == 0]
        image = self._read_image(image_id)
        if self.transform:
            image, boxes, labels = self.transform(image, boxes, labels)
        if self.target_transform:
            boxes, labels = self.target_transform(boxes, labels)
        targets = Container(
            boxes=boxes,
            labels=labels,
        )
        return image, targets, index

    def __len__(self):
        return len(self.ids)

    def _get_annotation(self, image_id):
        raise NotImplementedError

    def _read_image(self, image_id):
        raise NotImplementedError
