import os
from abc import ABC

import numpy as np
from PIL import Image

from .base import Base


class MOTDet(Base, ABC):
    class_names = ('__background__', 'person')

    def __init__(self, data_dir, mot_seq, transform=None, target_transform=None, keep_difficult=True):
        super().__init__(transform, target_transform, keep_difficult)
        seq_root = os.path.join(data_dir, mot_seq)
        self.seq_info = MOTDet._get_info(os.path.join(seq_root, "seqinfo.ini"))
        self.image_dir = os.path.join(seq_root, "img")
        self.annotation_dir = os.path.join(seq_root, "ann")
        self.ids = [image_id.replace(self.seq_info["imExt"], "") for image_id in os.listdir(self.image_dir)]
        self.class_dict = {class_name: i for i, class_name in enumerate(self.class_names)}

    def _get_annotation(self, image_id):
        ann_file = os.path.join(self.annotation_dir, f"{image_id}.txt")
        boxes = []
        labels = []
        with open(ann_file, "r") as f:
            for line in f.readlines():
                box_str = line.split(",")
                top = round(eval(box_str[0]), 2)
                left = round(eval(box_str[1]), 2)
                bottom = round(eval(box_str[2]), 2)
                right = round(eval(box_str[3]), 2)
                boxes.append([top, left, bottom, right])
                labels.append(self.class_dict["person"])
        return (np.array(boxes, dtype=np.float32),
                np.array(labels, dtype=np.int64),
                None)

    def _read_image(self, image_id):
        image_file = os.path.join(self.image_dir, f"{image_id}{self.seq_info['imExt']}")
        image = Image.open(image_file).convert("RGB")
        image = np.array(image)
        return image

    def get_annotation(self, index):
        image_id = self.ids[index]
        return image_id, self._get_annotation(image_id)

    def get_image(self, index):
        image_id = self.ids[index]
        return image_id, self._read_image(image_id)

    def __len__(self):
        return len(self.ids)

    @staticmethod
    def _get_info(file):
        info = {'imExt': ".jpg"}
        if not os.path.exists(file):
            return info

        with open(file, "r") as f:
            for line in f.readlines():
                line = line.strip("\n")
                if len(line.split('=')) == 2:
                    params = line.split('=')
                    try:
                        params[1] = eval(params[1])
                    except:
                        pass
                    info[params[0]] = params[1]
        return info

    def get_img_info(self, index):
        img_id = self.ids[index]
        image = self._read_image(img_id)
        return {"height": image.shape[0], "width": image.shape[1]}
