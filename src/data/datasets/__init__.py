from torch.utils.data import ConcatDataset

from src.config.path_catlog import DatasetCatalog
from .coco import COCODataset
from .mot import MOTDet
from .shop import SmartShop
from .voc import VOCDataset

_DATASETS = {
    'VOCDataset': VOCDataset,
    'COCODataset': COCODataset,
    'SmartShop': SmartShop,
    'MOTDet': MOTDet
}


def build_dataset(data_list, transform=None, target_transform=None, is_train=True):
    assert len(data_list) > 0
    dataset_list = []

    for dataset_name in data_list:
        data = DatasetCatalog.get(dataset_name)
        args = data['args']
        factory = _DATASETS[data['factory']]
        args['transform'] = transform
        args['target_transform'] = target_transform
        if factory == VOCDataset:
            args['keep_difficult'] = not is_train
        elif factory == COCODataset:
            args['remove_empty'] = is_train
        dataset = factory(**args)
        dataset_list.append(dataset)
    # for testing, return a list of datasets
    if not is_train:
        return dataset_list
    dataset = dataset_list[0]
    if len(dataset_list) > 1:
        dataset = ConcatDataset(dataset_list)

    return [dataset]
