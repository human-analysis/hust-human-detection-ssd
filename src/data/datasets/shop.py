import os
import xml.etree.ElementTree as ET

import numpy as np
from PIL import Image
from torch.utils.data import Dataset


class SmartShop(Dataset):
    class_names = ('__background__', 'person')

    def __init__(self, data_dir, split, transform=None, target_transform=None):
        """
        :param data_dir:
        :param split:
        :param transform:
        :param target_transform:
        """

        self.data_dir = data_dir
        self.image_path = os.path.join(self.data_dir, "images")
        self.annotation_path = os.path.join(self.data_dir, "annotations")
        self.ids = SmartShop._get_image_ids(os.path.join(self.data_dir, "dataset_ids", f"{split}.txt"))
        if len(self.ids) == 0:
            self.ids = os.listdir(self.image_path)
            self.ids = [path.replace(".jpg", "") for path in self.ids]
        self.split = split
        self.transform = transform
        self.target_transform = target_transform
        self.class_dict = {class_name: i for i, class_name in enumerate(SmartShop.class_names)}

    def __getitem__(self, index):
        image_id = self.ids[index]
        boxes, labels = self._get_annotation(image_id)
        image = self._read_image(image_id)
        if self.transform:
            image, boxes, labels = self.transform(image, boxes, labels)
        if self.target_transform:
            boxes, labels = self.target_transform(boxes, labels)
        container = {"boxes": boxes, "labels": labels}
        return image, container, index

    def __len__(self):
        return len(self.ids)

    def get_annotation(self, index):
        image_id = self.ids[index]
        return image_id, self._get_annotation(image_id)

    def parse_xml_file(self, filename):
        boxes = []
        labels = []
        tree = ET.parse(filename)
        for obj in tree.findall('object'):
            bbox = obj.find('bndbox')
            boxes.append([int(bbox.find('xmin').text),
                          int(bbox.find('ymin').text),
                          int(bbox.find('xmax').text),
                          int(bbox.find('ymax').text)])
            labels.append(self.class_dict["person"])
        return boxes, labels

    def _get_annotation(self, image_id):
        annotation_file = os.path.join(self.annotation_path, f"{image_id}.xml")
        boxes, labels = self.parse_xml_file(annotation_file)

        return (np.array(boxes, dtype=np.float32),
                np.array(labels, dtype=np.int64))

    def _read_image(self, image_id):
        image_file = os.path.join(self.image_path, f"{image_id}.jpg")
        assert os.path.exists(image_file) is True
        image = Image.open(image_file).convert("RGB")
        image = np.array(image)
        return image

    @staticmethod
    def _get_image_ids(data_file):
        ids = []
        if os.path.exists(data_file):
            with open(data_file, "r") as f:
                for line in f:
                    ids.append(line.rstrip())

        return ids

    def get_img_info(self, index):
        img_id = self.ids[index]
        image = self._read_image(img_id)
        return {"height": image.shape[0], "width": image.shape[1]}
