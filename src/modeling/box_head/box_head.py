import torch.nn.functional as F
from torch import nn

from src.losses import get_loss
from src.modeling import registry
from src.modeling.anchors.prior_box import PriorBox
from src.modeling.box_head.box_predictor_factory import make_box_predictor
from src.modeling.box_head.inference import SSDPostProcessor
from src.utils import box_utils


@registry.BOX_HEADS.register('SSDBoxHead')
class SSDBoxHead(nn.Module):
    def __init__(self, cfg):
        super().__init__()
        self.cfg = cfg
        self.predictor = make_box_predictor(cfg)
        self.loss_evaluator = get_loss(cfg)
        self.post_processor = SSDPostProcessor(cfg)
        self.priors = None

    def forward(self, features, targets=None):
        cls_logits, bbox_pred = self.predictor(features)
        if targets:
            return self._forward_train(cls_logits, bbox_pred, targets)
        else:
            return self._forward_test(cls_logits, bbox_pred)

    def _forward_train(self, cls_logits, bbox_pred, targets):
        gt_boxes, gt_labels = targets['boxes'], targets['labels']
        if self.cfg.SOLVER.BOXES_LOSS != "SmoothL1":
            if self.priors is None:
                self.priors = PriorBox(self.cfg)().to(bbox_pred.device)
        loss_dict = self.loss_evaluator(cls_logits, bbox_pred, gt_labels, gt_boxes, self.priors)
        detections = (cls_logits, bbox_pred)
        return detections, loss_dict

    def _forward_test(self, cls_logits, bbox_pred):
        if self.priors is None:
            self.priors = PriorBox(self.cfg)().to(bbox_pred.device)
        scores = F.softmax(cls_logits, dim=2)
        boxes = box_utils.convert_locations_to_boxes(
            bbox_pred, self.priors, self.cfg.MODEL.CENTER_VARIANCE, self.cfg.MODEL.SIZE_VARIANCE
        )
        boxes = box_utils.center_form_to_corner_form(boxes)
        detections = (scores, boxes)
        detections = self.post_processor(detections)
        return detections, {}
