from src.modeling import registry
from src.modeling.box_head.box_predictor import SSDBoxPredictor, SSDLiteBoxPredictor, EfficientBoxPredictor, \
    FSSDBoxPredictor, FSSDBoxLitePredictor

__all__ = ["make_box_predictor", "SSDLiteBoxPredictor", "SSDBoxPredictor", "FSSDBoxPredictor", "FSSDBoxLitePredictor",
           "EfficientBoxPredictor"]


def make_box_predictor(cfg):
    return registry.BOX_PREDICTORS[cfg.MODEL.BOX_PREDICTOR](cfg)
