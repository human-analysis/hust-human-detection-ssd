import torch.nn as nn

base = {
    'vgg16': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'C', 512, 512, 512, 'M',
              512, 512, 512],
}


# CONV_DEFS_16 = [
#     Conv(stride=1, depth=64),
#     Conv(stride=1, depth=64),
#     'M',
#     Conv(stride=1, depth=128),
#     Conv(stride=1, depth=128),
#     'M'
#     Conv(stride=1, depth=256),
#     Conv(stride=1, depth=256),
#     Conv(stride=1, depth=256),
#     'M'
#     Conv(stride=1, depth=512),
#     Conv(stride=1, depth=512),
#     Conv(stride=1, depth=512),
#     'M'
#     Conv(stride=1, depth=512),
#     Conv(stride=1, depth=512),
#     Conv(stride=1, depth=512),
# ]

# Conv = namedtuple('Conv', ['stride', 'depth'])

# class _conv_bn(nn.Module):
#     def __init__(self, inp, oup, stride):
#         super(_conv_bn, self).__init__()
#         self.conv = nn.Sequential(
#             nn.Conv2d(inp, oup, 3, stride, 1, bias=False),
#             nn.BatchNorm2d(oup),
#             nn.ReLU(inplace=True),
#         )
#         self.depth = oup

#     def forward(self, x):
#         return self.conv(x)


def vgg(cfg, i, batch_norm=False):
    layers = []
    in_channels = i
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        elif v == 'C':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2, ceil_mode=True)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    layers += [
        nn.MaxPool2d(kernel_size=3, stride=1, padding=1),
        nn.Conv2d(512, 1024, kernel_size=3, padding=6, dilation=6),
        nn.ReLU(inplace=True),
        nn.Conv2d(1024, 1024, kernel_size=1),
        nn.ReLU(inplace=True)]
    return layers


def _conv_dw(inp, oup, stride=1, padding=0, expand_ratio=1):
    return nn.Sequential(
        # pw
        nn.Conv2d(inp, oup * expand_ratio, 1, 1, 0, bias=False),
        nn.BatchNorm2d(oup * expand_ratio),
        nn.ReLU6(inplace=True),
        # dw
        nn.Conv2d(oup * expand_ratio, oup * expand_ratio, 3, stride, padding, groups=oup * expand_ratio, bias=False),
        nn.BatchNorm2d(oup * expand_ratio),
        nn.ReLU6(inplace=True),
        # pw-linear
        nn.Conv2d(oup * expand_ratio, oup, 1, 1, 0, bias=False),
        nn.BatchNorm2d(oup),
    )


def add_extras(feature_layer, version):
    extra_layers = []
    in_channels = None
    for layer, depth in zip(feature_layer[0], feature_layer[1]):
        if version == 'lite':
            if layer == 'S':
                extra_layers += [_conv_dw(in_channels, depth, stride=2, padding=1, expand_ratio=1)]
                in_channels = depth
            elif layer == '':
                extra_layers += [_conv_dw(in_channels, depth, stride=1, expand_ratio=1)]
                in_channels = depth
            else:
                in_channels = depth
        else:
            if layer == 'S':
                extra_layers += [
                    nn.Conv2d(in_channels, int(depth / 2), kernel_size=1),
                    nn.Conv2d(int(depth / 2), depth, kernel_size=3, stride=2, padding=1)]
                in_channels = depth
            elif layer == '':
                extra_layers += [
                    nn.Conv2d(in_channels, int(depth / 2), kernel_size=1),
                    nn.Conv2d(int(depth / 2), depth, kernel_size=3)]
                in_channels = depth
            else:
                in_channels = depth
    return extra_layers


class VGG16(nn.Module):
    def __init__(self, cfg, version=''):
        super().__init__()
        self.cfg = cfg
        self.base = nn.ModuleList(vgg(base['vgg16'], 3))
        self.indices = [22, 34]
        self.version = version
        self.extras = nn.ModuleList(add_extras(cfg.MODEL.BACKBONE.FEATURE_LAYERS[0], version))

    def forward(self, inputs):
        features = []
        for i, layer in enumerate(self.base):
            inputs = layer(inputs)
            if i in self.indices:
                features.append(inputs)

        for i, layer in enumerate(self.extras):
            inputs = layer(inputs)
            if i % 2 == 1 or self.version == "lite":
                features.append(inputs)
        return features
