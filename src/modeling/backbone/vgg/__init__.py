import os
from pathlib import Path

import torch

from src.modeling import registry
from src.modeling.backbone.vgg.vgg import VGG16

model_urls = {
    'vgg16': os.path.join(Path(__file__).parent.parent.parent.parent.parent,
                          "model_zoo/vgg16_fssd_voc_77.8.pth"),
}


@registry.BACKBONES.register('vgg16')
def vgg16(cfg, pretrained=True):
    model = VGG16(cfg)
    if pretrained:
        pretrained_dict = torch.load(model_urls['vgg16'], map_location="cpu")
        model_dict = model.state_dict()

        pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
        model_dict.update(pretrained_dict)
        model.load_state_dict(model_dict)
    return model


@registry.BACKBONES.register('vgg16lite')
def vgg16(cfg, pretrained=True):
    model = VGG16(cfg, version='lite')
    if pretrained:
        pretrained_dict = torch.load(model_urls['vgg16'], map_location="cpu")
        model_dict = model.state_dict()

        pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
        model_dict.update(pretrained_dict)
        model.load_state_dict(model_dict)
    return model
