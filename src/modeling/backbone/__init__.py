from src.modeling import registry
from src.modeling.backbone.efficient_net import efficient_net_b0, efficient_lite_b0, efficient_lite_b1, \
    efficient_lite_b2, efficient_lite_b3, \
    efficient_lite_b4
from src.modeling.backbone.mobilenet import MobileNetV2, MobileNetV3, mobilenet_v2, mobilenetv3_small_voc
from src.modeling.backbone.vgg import vgg16

__all__ = ['build_backbone', 'MobileNetV2', 'MobileNetV3', 'efficient_net_b0', 'efficient_lite_b0', 'efficient_lite_b1',
           'efficient_lite_b2', 'efficient_lite_b3', 'efficient_lite_b4', 'vgg16']


def build_backbone(cfg):
    return registry.BACKBONES[cfg.MODEL.BACKBONE.NAME](cfg, cfg.MODEL.BACKBONE.PRETRAINED)
