import cv2
import os
from glob import glob
from tqdm import tqdm 

temp = "./temps/"
image_paths = os.listdir('./img')

def draw_boxes(image, boxes):
    for box in boxes:
        cv2.rectangle(image, (box[0], box[1]), (box[2], box[3]), (255, 0, 0), 1)
    return image

for path in tqdm(image_paths): 
    if not os.path.exists(os.path.join("img", path)):
        continue
    
    image = cv2.imread(os.path.join("img", path))
    
    if image is None:
        continue
    w, h, _ = image.shape
    
    boxes = []
    with open(os.path.join("ann", path.replace("jpg", "txt"))) as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip('\n').strip()
            params = line.split(',')
            params = [param for param in params if param != '']
            x1 = eval(params[0])  
            x2 = eval(params[1]) 
            x3 = eval(params[2])
            x4 = eval(params[3])
            boxes.append([int(x1), int(x2), int(x3), int(x4)])
    image = draw_boxes(image, boxes)
    cv2.imwrite(os.path.join("temps", path), image)
