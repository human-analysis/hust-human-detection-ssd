import glob
import logging
import os
import shutil

import click
import cv2
import numpy as np
from tqdm import tqdm

from src.utils.data_utils import check_inside_box, get_boxes_from_file

logger = logging.getLogger(__name__)
ix, iy, ex, ey = -1, -1, -1, -1
frame_sz = (1280, 720)
x_scale, y_scale = 1.0, 1.0
cap_from_stream = True
is_continue = True


def draw_rec(event, x, y, flags, param):
    global ix, iy, ex, ey

    if event == cv2.EVENT_LBUTTONDOWN:
        ix, iy = x, y

    elif event == cv2.EVENT_LBUTTONUP:
        ex, ey = x, y
        cv2.rectangle(param, (ix, iy), (x, y), (0, 255, 0), 0)


def get_crop_image(img_file):
    img = cv2.imread(img_file)
    global ix, iy, ex, ey, x_scale, y_scale, is_continue
    ix, iy, ex, ey = 0, 0, img.shape[0], img.shape[1]

    while True:
        frame = cv2.imread(img_file)
        if cap_from_stream:
            x_scale = frame_sz[0] / frame.shape[0]
            y_scale = frame_sz[1] / frame.shape[1]
            frame = cv2.resize(frame, frame_sz)
        cv2.namedWindow("draw_rectangle")
        cv2.setMouseCallback("draw_rectangle", draw_rec, frame)
        logger.info("Choose your area of interest!")
        is_redraw = False
        while 1:
            cv2.imshow('draw_rectangle', frame)
            k = cv2.waitKey(1) & 0xFF
            if k == ord('a'):
                cv2.destroyAllWindows()
                break
            elif k == ord('r'):
                cv2.destroyAllWindows()
                is_redraw = True
                break
            elif k == ord('c'):
                cv2.destroyAllWindows()
                is_continue = False
                break
        if not is_redraw:
            break


def save_img_ann(temp_dir, file, img, filtered_boxes, is_ann):
    temp_img_dir = f"{temp_dir}/img1/"
    temp_ann_dir = f"{temp_dir}/ann/"

    cv2.imwrite(f"{temp_img_dir}/{file.split('/')[-1]}", img)
    if len(filtered_boxes) == 0:
        return
    if is_ann:
        with open(os.path.join(temp_ann_dir, f"{file.split('/')[-1].replace('jpg', 'txt')}"), 'w') as f:
            for i, box in enumerate(filtered_boxes):
                f.write(",".join([str(x) for x in box]))
                if i < len(filtered_boxes) - 1:
                    f.write("\n")


@click.command()
@click.option("--data_dir", default="datasets/mot/train/", help="Root path of your dataset")
@click.option("--mot_seq", required=True, help="You should input your mot-dataset such that 'MOT17-02-DPM'")
@click.option("--filter_threshold", default=0.5, help="You should keep greater-threshold boxes")
@click.option("--pass_step", default=3, help="Pass step image to prevent duplicating data")
@click.option("--version", default=0)
@click.option("--is_resize", default=False)
@click.option("--is_ann", default=False)
@click.option("--limit_person", type=int, default=10)
def crop_crowded_scene(data_dir, mot_seq, filter_threshold, pass_step, version, is_resize, is_ann, limit_person):
    global ix, iy, ex, ey, x_scale, y_scale, cap_from_stream, is_continue
    cap_from_stream = is_resize

    sequence_root = os.path.join(data_dir, mot_seq)
    image_dir = os.path.join(sequence_root, "img1")
    temp_dir = f"datasets/tmp/{mot_seq}_ver{version}/"

    annotation_file = os.path.join(sequence_root, "gt", "gt.txt")
    image_files = glob.glob(os.path.join(image_dir, "*.jpg"))

    if os.path.exists(temp_dir):
        # files = os.listdir(f"{temp_dir}/img1/")
        # for file in files:
        #     os.remove(os.path.join(f"{temp_dir}/img1/", file))
        #
        # files = os.listdir(f"{temp_dir}/ann/")
        # for file in files:
        #     os.remove(os.path.join(f"{temp_dir}/ann/", file))
        shutil.rmtree(temp_dir)

    os.makedirs(os.path.join(temp_dir, "img1"), exist_ok=True)
    os.makedirs(os.path.join(temp_dir, "ann"), exist_ok=True)

    info = {"crop_size": [0, 0], "region_area": [ix, iy, ex, ey]}
    logger.info(f"Before cropping: {info['region_area']}")

    if os.path.exists(f"{temp_dir}/info.ini"):
        with open(os.path.join(temp_dir, "info.ini"), "r") as f:
            for line in f.readlines():
                line = line.strip("\n")
                if len(line.split("=")) == 1:
                    continue
                info[line.split("=")[0]] = line.split("=")[1]
    else:
        get_crop_image(image_files[0])
        info["crop_size"] = [ex - ix, ey - iy]
        info["region_area"] = [ix, iy, ex, ey]
        with open(os.path.join(temp_dir, "info.ini"), "w") as f:
            f.write("[cropping_parameter]\n")
            for key in info.keys():
                f.write(f"{key}={info[key]}\n")

    if is_continue is False:
        return
    logger.info(f"After get interest of region: {(ix, iy, ex, ey)}")

    try:
        frame_boxes_dict, num_boxes = get_boxes_from_file(annotation_file)
    except:
        frame_boxes_dict, num_boxes = {}, 0
    for i, file in tqdm(enumerate(image_files)):
        if i % pass_step != 0:
            continue
        img = cv2.imread(file, 3)

        y_ = img.shape[0]
        x_ = img.shape[1]
        x_scale = frame_sz[0] / x_
        y_scale = frame_sz[1] / y_

        if cap_from_stream:
            img = cv2.resize(img, frame_sz)

        img = img[iy:ey, ix:ex, :]

        try:
            boxes = frame_boxes_dict[file.split("/")[-1].replace(".jpg", "")]
        except:
            boxes = []

        filtered_boxes = []
        for box in boxes:
            if cap_from_stream:
                box[0] = int(np.round(box[0] * x_scale))
                box[1] = int(np.round(box[1] * y_scale))
                box[2] = int(np.round(box[2] * x_scale))
                box[3] = int(np.round(box[3] * y_scale))

            if not check_inside_box([ix, iy, ex, ey], filter_threshold, box[:4]):
                continue

            box[0] = max(box[0] - ix, 0)
            box[1] = max(box[1] - iy, 0)
            box[2] = max(box[2] - ix, 0)
            box[3] = max(box[3] - iy, 0)
            filtered_boxes.append(box)

        save_img_ann(temp_dir, file, img, filtered_boxes, is_ann)

    logger.info("Starting to filter outside boxes from file")
    logger.info("Everything is done!")


@click.command()
@click.option("--data_dir", required=True)
@click.option("--processed_dir", required=True)
def create_processed_data(data_dir, processed_dir):
    os.makedirs(os.path.join(processed_dir, "img1"), exist_ok=True)
    os.makedirs(os.path.join(processed_dir, "ann"), exist_ok=True)

    seqs = os.listdir(data_dir)
    for seq in tqdm(seqs):
        seq_dir = os.path.join(data_dir, seq)
        if os.path.exists(os.path.join(seq_dir, "img1")) is False:
            continue
        files = os.listdir(os.path.join(seq_dir, "img1"))
        for file in files:
            shutil.copy2(os.path.join(os.path.join(seq_dir, "img1"), file), f"{processed_dir}/img1/{seq}_{file}")


def get_boxes(ann_file):
    assert os.path.exists(ann_file) == True
    boxes = []
    with open(ann_file, "r") as f:
        for line in f.readlines():
            line = line.strip('\n')
            params = line.split(",")
            if len(params) != 5:
                continue
            w = int(params[2]) - int(params[0])
            h = int(params[3]) - int(params[1])
            if int(params[1]) <= 10 and w * 1.5 > h:
                continue
            if float(params[4]) >= 0:
                boxes.append([int(params[0]), int(params[1]), int(params[2]), int(params[3]), float(params[4])])
    return boxes


def write_boxes(ann_file, boxes):
    with open(ann_file, "w") as f:
        for i, box in enumerate(boxes):
            f.write(",".join([str(x) for x in box]))
            if i < len(boxes) - 1:
                f.write("\n")


def draw_boxes(image, boxes):
    for box in boxes:
        cv2.rectangle(image, (box[0], box[1]), (box[2], box[3]), (255, 0, 0), 1)
        text = "{:.2f}".format(box[4])
        cv2.putText(image, text, (box[0], box[1] - 5), cv2.FONT_HERSHEY_SIMPLEX,
                    0.4, (255, 0, 0), 1)
    return image


@click.command()
@click.option("--data_dir", required=True)
def filter_low_square_boxes(data_dir):
    # os.makedirs(os.path.join(data_dir, "debugging_img"), exist_ok=True)
    # os.makedirs(os.path.join(data_dir, "debugging_ann"), exist_ok=True)
    # ids = [id.replace(".jpg", "") for id in os.listdir(os.path.join(data_dir, "img1"))]
    # for img_id in tqdm(ids):
    #     image_file = os.path.join(data_dir, f"img1/{img_id}.jpg")
    #     ann_file = os.path.join(data_dir, f"ann/{img_id}.txt")
    #     print(image_file, ann_file)
    #     image = cv2.imread(image_file)
    #
    #     boxes = get_boxes(ann_file)
    #     if len(boxes) == 0:
    #         continue
    #
    #     image = draw_boxes(image, boxes)
    #     cv2.imwrite(f"{os.path.join(data_dir, 'debugging_img')}/{img_id}.jpg", image)
    #     write_boxes(f"{os.path.join(data_dir, 'debugging_ann')}/{img_id}.txt", boxes)
    os.makedirs(os.path.join("check", "img1"), exist_ok=True)
    os.makedirs(os.path.join("check", "ann"), exist_ok=True)
    bboxes = os.path.join(data_dir, "bbox")
    ann_dir = os.path.join(data_dir, "ann")
    original_dir = os.path.join(data_dir, "img1")
    for ids in tqdm(os.listdir(bboxes)):
        img_file = os.path.join(original_dir, ids)
        ann_file = os.path.join(ann_dir, ids.replace("jpg", "txt"))
        shutil.copy2(img_file, os.path.join("check", "img1", img_file.split("/")[-1]))
        shutil.copy2(ann_file, os.path.join("check", "ann", ann_file.split("/")[-1]))


if __name__ == "__main__":
    # crop_crowded_scene()
    # create_processed_data()
    filter_low_square_boxes()
