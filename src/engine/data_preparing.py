import logging
import os

import click

from src.utils.data_utils import retrieve_boxes_from_file

logger = logging.getLogger(__name__)


@click.command()
@click.option("--data_dir", required=True, help="Root path of your dataset")
@click.option("--mot_seq", required=True, help="You should input your mot-dataset such that 'MOT17-02-DPM'")
def mot_preparing(data_dir, mot_seq):
    sequence_root = os.path.join(data_dir, mot_seq)
    annotation_file = os.path.join(sequence_root, "gt", "gt.txt")
    retrieve_boxes_from_file(sequence_root, annotation_file)
    logger.info("Everything is done!")


if __name__ == "__main__":
    mot_preparing()
