import os
from pathlib import Path


class DatasetCatalog:
    DATA_DIR = os.path.join(Path(__file__).parent.absolute().parent.parent, "datasets")
    DATASETS = {
        'voc_2007_train': {
            "data_dir": "VOC2007",
            "split": "train"
        },
        'voc_2007_val': {
            "data_dir": "VOC2007",
            "split": "val"
        },
        'voc_2007_trainval': {
            "data_dir": "VOC2007",
            "split": "trainval"
        },
        'voc_2007_test': {
            "data_dir": "VOC2007",
            "split": "test"
        },
        'voc_2012_train': {
            "data_dir": "VOC2012",
            "split": "train"
        },
        'voc_2012_val': {
            "data_dir": "VOC2012",
            "split": "val"
        },
        'voc_2012_trainval': {
            "data_dir": "VOC2012",
            "split": "trainval"
        },
        'voc_2012_test': {
            "data_dir": "VOC2012",
            "split": "test"
        },
        'coco_2014_valminusminival': {
            "data_dir": "val2014",
            "ann_file": "annotations/instances_valminusminival2014.json"
        },
        'coco_2014_minival': {
            "data_dir": "val2014",
            "ann_file": "annotations/instances_minival2014.json"
        },
        'coco_2014_train': {
            "data_dir": "train2014",
            "ann_file": "annotations/instances_train2014.json"
        },
        'coco_2014_val': {
            "data_dir": "val2014",
            "ann_file": "annotations/instances_val2014.json"
        },
        'smart_shop_train': {
            "data_dir": "smart_shop",
            "split": "train"
        },
        'smart_shop_val': {
            "data_dir": "smart_shop",
            "split": "val"
        },
        'sample_mot_train': {"data_dir": "custom_data", "split": "train"},
        'cuhk_camera_train': {"data_dir": "custom_data", "split": "train"},
        'cuhk_direct_train': {"data_dir": "custom_data", "split": "train"},
        'sample_mot_test': {"data_dir": "custom_data", "split": "test"},
        'cuhk_camera_test': {"data_dir": "custom_data", "split": "test"},
        'cuhk_direct_test': {"data_dir": "custom_data", "split": "test"},
    }

    @staticmethod
    def get(name):
        if "voc" in name:
            voc_root = DatasetCatalog.DATA_DIR
            if 'VOC_ROOT' in os.environ:
                voc_root = os.environ['VOC_ROOT']

            attrs = DatasetCatalog.DATASETS[name]
            args = dict(
                data_dir=os.path.join(voc_root, attrs["data_dir"]),
                split=attrs["split"],
            )
            return dict(factory="VOCDataset", args=args)
        elif "coco" in name:
            coco_root = DatasetCatalog.DATA_DIR
            if 'COCO_ROOT' in os.environ:
                coco_root = os.environ['COCO_ROOT']

            attrs = DatasetCatalog.DATASETS[name]
            args = dict(
                data_dir=os.path.join(coco_root, attrs["data_dir"]),
                ann_file=os.path.join(coco_root, attrs["ann_file"]),
            )
            return dict(factory="COCODataset", args=args)

        elif "smart_shop_train" in name:
            shop_root = DatasetCatalog.DATA_DIR
            attrs = DatasetCatalog.DATASETS[name]
            args = dict(
                data_dir=os.path.join(shop_root, attrs["data_dir"]),
                split=attrs["split"],
            )
            return dict(factory="SmartShop", args=args)

        elif "smart_shop_val" in name:
            shop_root = DatasetCatalog.DATA_DIR
            attrs = DatasetCatalog.DATASETS[name]
            args = dict(
                data_dir=os.path.join(shop_root, attrs["data_dir"]),
                split=attrs["split"],
            )
            return dict(factory="SmartShop", args=args)

        elif name in ['sample_mot_train', 'sample_mot_test',
                      'cuhk_camera_train', 'cuhk_camera_test',
                      'cuhk_direct_train', 'cuhk_direct_test']:
            attrs = DatasetCatalog.DATASETS[name]
            project_root = Path(__file__).parent.parent.parent
            args = dict(
                data_dir=os.path.join(project_root, DatasetCatalog.DATA_DIR, attrs["data_dir"], attrs["split"]),
                mot_seq=name
            )
            return dict(factory="MOTDet", args=args)

        raise RuntimeError("Dataset not available: {}".format(name))
