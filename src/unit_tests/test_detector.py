import unittest

import torch

from src.config.defaults import _C as cfg
from src.modeling.detector import build_detection_model
from src.utils import count_parameters


class TestDetector(unittest.TestCase):
    def testEfficientNet(self):
        cfg.merge_from_file(
            "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/configs/advprop_efficient_net_b0_ssd300.yaml")
        # "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/configs/mobilenet_v3_small_ssd320.yaml")
        model = build_detection_model(cfg)
        inputs = torch.rand(2, 3, 300, 300)
        print(model(inputs))
        print(count_parameters(model))

    def testMobilenetv2FSSD(self):
        cfg.merge_from_file(
            "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/configs/mobilenet_v2_fssd300.yaml")
        model = build_detection_model(cfg)
        inputs = torch.rand(2, 3, 300, 300)
        print(model(inputs))
        print(count_parameters(model))

    def testDetectionConfig(self):
        feature_layer = [[[8, 12, 16], [256, 512, 1024]],
                         [['', 'S', 'S', 'S', '', ''], [512, 512, 256, 256, 256, 256]]]
        for layer, depth in zip(feature_layer[0][0], feature_layer[0][1]):
            print(layer, depth)

    def testVGGFSSD(self):
        cfg.merge_from_file(
            "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/configs/vgg16_fssd300.yaml")
        model = build_detection_model(cfg)
        inputs = torch.rand(2, 3, 300, 300)
        print(model(inputs))
        print(count_parameters(model))

    def testEfficientFSSD(self):
        cfg.merge_from_file(
            "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/configs/advprop_efficient_net_b0_fssd300.yaml")
        model = build_detection_model(cfg)
        inputs = torch.rand(2, 3, 300, 300)
        print(model(inputs))
        print(count_parameters(model))

    def testMobilenetv3SSD(self):
        cfg.merge_from_file(
            "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/configs/mobilenet_v3_small_ssdlite320.yaml")
        model = build_detection_model(cfg)
        inputs = torch.rand(2, 3, 320, 320)
        print(model(inputs))
        print(count_parameters(model))

    def testMobilenetv3FSSD(self):
        cfg.merge_from_file(
            "/mnt/D466DA6A66DA4CBC/2020/schools/efficientnet-detection/configs/mobilenet_v3_small_fssdlite320.yaml")
        model = build_detection_model(cfg)
        inputs = torch.rand(2, 3, 320, 320)
        print(model(inputs))
        print(count_parameters(model))
