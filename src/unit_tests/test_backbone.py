import unittest

import torch

from src.config.defaults import _C as cfg
from src.modeling.backbone.efficient_net import efficient_lite_b0, efficient_net_b0
from src.modeling.backbone.mobilenet.mobilenet_v3 import mobilenetv3_small
from src.modeling.backbone.vgg import VGG16
from src.utils import count_parameters


class TestBackbone(unittest.TestCase):
    def testEfficientNetLite(self):
        inputs = torch.rand(2, 3, 300, 300)
        model = efficient_lite_b0(cfg)
        print(count_parameters(model))
        # features = model(inputs)
        # for i, feat in enumerate(features):
        #     print(feat.size())
        # torch.Size([2, 40, 38, 38])
        # torch.Size([2, 112, 19, 19])
        # torch.Size([2, 320, 10, 10])
        # torch.Size([2, 256, 5, 5])
        # torch.Size([2, 256, 3, 3])
        # torch.Size([2, 256, 1, 1])

    def testEfficientNet(self):
        inputs = torch.rand(2, 3, 300, 300)
        model = efficient_net_b0(cfg)
        features = model(inputs)
        for i, feat in enumerate(features):
            print(feat.size())
        # torch.Size([2, 40, 38, 38])
        # torch.Size([2, 112, 19, 19])
        # torch.Size([2, 320, 10, 10])
        # torch.Size([2, 256, 5, 5])
        # torch.Size([2, 256, 3, 3])
        # torch.Size([2, 256, 1, 1])

    def testAdvEfficientNet(self):
        inputs = torch.rand(2, 3, 300, 300)
        cfg.MODEL.BACKBONE.ADVPROP = True
        model = efficient_net_b0(cfg)
        features = model(inputs)
        for i, feat in enumerate(features):
            print(feat.size())
        # torch.Size([2, 40, 38, 38])
        # torch.Size([2, 112, 19, 19])
        # torch.Size([2, 320, 10, 10])
        # torch.Size([2, 256, 5, 5])
        # torch.Size([2, 256, 3, 3])
        # torch.Size([2, 256, 1, 1])

    def testMobileNet(self):
        inputs = torch.rand(2, 3, 320, 320)
        # model = MobileNetV2()
        # print(count_parameters(model))
        # features = model(inputs)
        # for feat in features:
        #     print(feat.size())
        # 0 torch.Size([2, 96, 19, 19])
        # 1 torch.Size([2, 1280, 10, 10])
        # 2 torch.Size([2, 512, 5, 5])
        # 3 torch.Size([2, 256, 3, 3])
        # 4 torch.Size([2, 256, 2, 2])
        # 5 torch.Size([2, 64, 1, 1])

        model = mobilenetv3_small()
        features = model.extract_feature(inputs)
        # print(count_parameters(model))

        # 0 torch.Size([2, 96, 10, 10])
        # 1 torch.Size([2, 1280, 10, 10])
        # 2 torch.Size([2, 512, 5, 5])
        # 3 torch.Size([2, 256, 3, 3])
        # 4 torch.Size([2, 256, 2, 2])
        # 5 torch.Size([2, 64, 1, 1])

    def testVGG(self):
        inputs = torch.rand(2, 3, 300, 300)
        model = VGG16(cfg, version='lite')
        features = model(inputs)
        for feat in features:
            print(feat.size())
