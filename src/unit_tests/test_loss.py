import unittest

import numpy as np
import torch
from yacs.config import CfgNode as CN

from src.config.defaults import _C as cfg
from src.losses import *
from src.modeling.anchors.prior_box import PriorBox


class TestLoss(unittest.TestCase):
    batch, n_anchors, num_classes = 2, 8732, 2
    pred_confs = torch.rand(batch, n_anchors, num_classes)
    pred_locs = torch.rand(batch, n_anchors, 4)
    target_labels = torch.tensor(np.random.randint(1, num_classes, (batch, n_anchors)))
    target_locs = torch.rand(batch, n_anchors, 4)
    boxes_loss_cfg = CN()
    boxes_loss_cfg.type = "Diou"
    boxes_loss_cfg.variance = (0.1, 0.2)
    priors = PriorBox(cfg)()

    def testMultiBoxLoss(self):
        neg_pos_ratio = 3
        loss_fn = MultiBoxLoss(num_classes=TestLoss.num_classes, boxes_loss_cfg=TestLoss.boxes_loss_cfg,
                               neg_pos_ratio=neg_pos_ratio)
        print(loss_fn(TestLoss.pred_confs, TestLoss.pred_locs, TestLoss.target_labels, TestLoss.target_locs,
                      TestLoss.priors))

    def testFocalLoss(self):
        neg_pos_ratio = 3
        gamma = 2
        alpha = 0.25
        loss_fn = FocalLoss(num_classes=TestLoss.num_classes, boxes_loss_cfg=TestLoss.boxes_loss_cfg,
                            neg_pos_ratio=neg_pos_ratio, gamma=gamma, alpha=alpha)
        print(loss_fn(TestLoss.pred_confs, TestLoss.pred_locs, TestLoss.target_labels, TestLoss.target_locs,
                      TestLoss.priors))

    def testMultiBoxRepulsionLoss(self):
        neg_pos_ratio = 3
        loss_fn = RepulsionMultiBoxLoss(num_classes=TestLoss.num_classes, boxes_loss_cfg=TestLoss.boxes_loss_cfg,
                                        neg_pos_ratio=neg_pos_ratio)
        print(loss_fn(TestLoss.pred_confs, TestLoss.pred_locs, TestLoss.target_labels, TestLoss.target_locs,
                      TestLoss.priors))

    def testFocalRepulsionLoss(self):
        neg_pos_ratio = 3
        loss_fn = RepulsionFocalLoss(num_classes=TestLoss.num_classes, boxes_loss_cfg=TestLoss.boxes_loss_cfg,
                                     neg_pos_ratio=neg_pos_ratio)
        print(loss_fn(TestLoss.pred_confs, TestLoss.pred_locs, TestLoss.target_labels, TestLoss.target_locs,
                      TestLoss.priors))
