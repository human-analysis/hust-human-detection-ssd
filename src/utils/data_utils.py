import logging
import os

logger = logging.getLogger(__name__)


def get_area(top_left, bottom_right):
    hw = [x - y for x, y in zip(bottom_right, top_left)]
    hw = [max(x, 0.0) for x in hw]
    area = hw[0] * hw[1]
    return area


def check_inside_box(area, iou_threshold, box):
    top_left = [max(x, y) for x, y in zip(box[:2], area[:2])]
    bottom_right = [min(x, y) for x, y in zip(box[2:], area[2:])]
    overlap_area = get_area(top_left, bottom_right)
    iou = overlap_area / (get_area(box[:2], box[2:]) + 1e-6)
    if iou >= iou_threshold:
        return True
    return False


def get_boxes_from_annotation(annotation_file):
    boxes = []
    with open(annotation_file, "r") as f:
        for line in f.readlines():
            params = line.split(",")
            boxes.append([round(float(x)) for x in params[:4]])
    return boxes


def get_boxes_from_file(gt_file, size_margin=None):
    num_boxes = 0
    with open(gt_file, "r") as f:
        frame_boxes_dict = {}
        for line in f.readlines():
            params = line.split(",")
            params[0] = params[0].zfill(6)
            if frame_boxes_dict.get(params[0], None) is None:
                frame_boxes_dict[params[0]] = []
            if (float(params[8]) >= 0.0 or float(params[8]) == -1.0) and (float(params[4]) * 3 > float(params[5])):
                if size_margin:
                    if float(params[4]) > size_margin[0] / 5:
                        continue
                if float(params[5]) > 280:
                    continue
                box = [float(params[2]), float(params[3]), float(params[2]) + float(params[4]),
                       float(params[3]) + float(params[5]), int(params[1]), float(params[8])]
                num_boxes += 1
                frame_boxes_dict[params[0]].append(box)
    return frame_boxes_dict, num_boxes


def retrieve_boxes_from_file(processed_root, annotation_file, box_filter_fn=None):
    frame_boxes_dict, num_boxes = get_boxes_from_file(annotation_file)
    num_filtered_boxes = 0
    for frame_id in frame_boxes_dict:
        os.makedirs(os.path.join(processed_root, "ann"), exist_ok=True)
        with open(os.path.join(processed_root, "ann", f"{frame_id}.txt"), "w") as f:
            for i, box in enumerate(frame_boxes_dict[frame_id]):
                if box_filter_fn is not None:
                    if not box_filter_fn(box):
                        num_filtered_boxes += 1
                        continue
                f.write(",".join([str(x) for x in box]))
                if i < len(frame_boxes_dict[frame_id]) - 1:
                    f.write("\n")
    logger.info(
        f"Firstly you have {num_boxes} boxes, after box filtering (or not) remaining to {num_filtered_boxes} boxes!")


if __name__ == "__main__":
    check_inside_box([100, 100, 1000, 1000], 0.5, [200, 50, 400, 400])
