from yacs.config import CfgNode as CN

from src.losses.focal_loss import FocalLoss
from src.losses.focal_repulsion_loss import RepulsionFocalLoss
from src.losses.multibox_loss import MultiBoxLoss
from src.losses.multibox_repulsion_loss import RepulsionMultiBoxLoss

__all__ = ["get_loss", "get_boxes_loss_cfg", "MultiBoxLoss", "FocalLoss", "RepulsionFocalLoss", "RepulsionMultiBoxLoss"]


def get_boxes_loss_cfg(cfg):
    boxes_loss_params = CN()
    boxes_loss_params.type = cfg.SOLVER.BOXES_LOSS
    if cfg.SOLVER.BOXES_LOSS in ["Iou", "Giou", "Diou", "Ciou"]:
        boxes_loss_params.variance = (cfg.MODEL.CENTER_VARIANCE, cfg.MODEL.SIZE_VARIANCE)
    return boxes_loss_params


def get_loss(cfg):
    return globals().get(cfg.SOLVER.LOSS)(cfg.MODEL.NUM_CLASSES, get_boxes_loss_cfg(cfg), **cfg.SOLVER.LOSS_PARAMS)
