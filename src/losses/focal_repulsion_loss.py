from src.losses.abtraction_loss import AbstractionLoss
from src.losses.focal_loss import FocalLoss
from src.losses.multibox_repulsion_loss import RepulsionMultiBoxLoss


class RepulsionFocalLoss(AbstractionLoss):
    def __init__(self, num_classes, boxes_loss_cfg, neg_pos_ratio=3, gamma=2, alpha=0.25,
                 use_gpu=True, *args, **kwargs):
        super().__init__(num_classes, use_gpu, *args, **kwargs)
        # TODO: inherit from both repulsion loss and focal loss
        self.focal_loss = FocalLoss(num_classes, boxes_loss_cfg, neg_pos_ratio, gamma, alpha, use_gpu, *args, **kwargs)
        self.repulsion_loss = RepulsionMultiBoxLoss(num_classes, boxes_loss_cfg, neg_pos_ratio, use_gpu, *args,
                                                    **kwargs)

    def get_conf_fn(self):
        return self.focal_loss.get_focal_loss

    def get_loc_fn(self):
        return self.repulsion_loss.get_repGT_loss


if __name__ == "__main__":
    loss_fn = RepulsionFocalLoss(num_classes=2)
    print(loss_fn.get_conf_fn())
