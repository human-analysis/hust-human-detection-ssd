import torch
import torch.nn as nn

from src.losses.utils import decode
from src.utils.box_utils import bbox_overlaps_iou, bbox_overlaps_giou, bbox_overlaps_diou, bbox_overlaps_ciou


class IouLoss(nn.Module):

    def __init__(self, pred_mode='Center', size_sum=True, variances=None, boxes_loss='Giou'):
        super(IouLoss, self).__init__()
        self.size_sum = size_sum
        self.pred_mode = pred_mode
        self.variances = variances
        self.loss = boxes_loss

    def forward(self, loc_p, loc_t, prior_data):
        """
        both lop_p and loc_t are center form boxes
        """
        num = loc_p.shape[0]

        if self.pred_mode == 'Center':
            decoded_prediction_boxes = decode(loc_p, prior_data, self.variances)
            decoded_truth_boxes = decode(loc_t, prior_data, self.variances)
        else:
            decoded_prediction_boxes = loc_p
            decoded_truth_boxes = loc_t

        if self.loss == 'Iou':
            loss = torch.sum(1.0 - bbox_overlaps_iou(decoded_prediction_boxes, decoded_truth_boxes))
        else:
            if self.loss == 'Giou':
                loss = torch.sum(1.0 - bbox_overlaps_giou(decoded_prediction_boxes, decoded_truth_boxes))
            else:
                if self.loss == 'Diou':
                    loss = torch.sum(1.0 - bbox_overlaps_diou(decoded_prediction_boxes, decoded_truth_boxes))
                else:
                    loss = torch.sum(1.0 - bbox_overlaps_ciou(decoded_prediction_boxes, decoded_truth_boxes))

        if self.size_sum:
            loss = loss
        else:
            loss = loss / num
        return loss
