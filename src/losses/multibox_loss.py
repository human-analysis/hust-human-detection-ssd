from abc import ABC

import torch
import torch.nn.functional as F

from src.losses.abtraction_loss import AbstractionLoss
from src.losses.iou_losses import IouLoss
from src.losses.utils import hard_negative_mining


class MultiBoxLoss(AbstractionLoss, ABC):
    def __init__(self, num_classes, boxes_loss_cfg, neg_pos_ratio, use_gpu=True, *args, **kwargs):
        super().__init__(num_classes, use_gpu, *args, **kwargs)
        self.neg_pos_ratio = neg_pos_ratio
        self.boxes_loss = boxes_loss_cfg.type
        self.g_iou = None
        if self.boxes_loss != "SmoothL1":
            self.g_iou = IouLoss(pred_mode='Center', size_sum=True, variances=boxes_loss_cfg.variance,
                                 boxes_loss=self.boxes_loss)

    def get_conf_fn(self):
        return self.get_confidence_loss

    def get_loc_fn(self):
        return self.get_regression_loss

    def get_confidence_loss(self, confidence, labels):
        # batch_conf = confidence.view(-1, confidence.size(2))
        # gathered_conf = batch_conf.gather(1, labels.view(-1, 1))
        # loss_c = log_sum_exp(batch_conf) - gathered_conf
        with torch.no_grad():
            loss_c = -F.log_softmax(confidence, dim=2)[:, :, 0]
            mask = hard_negative_mining(loss_c, labels, self.neg_pos_ratio)

        conf_p = confidence[mask].view(-1, confidence.size(2))
        targets_weighted = labels[mask]

        conf_loss = F.cross_entropy(conf_p, targets_weighted, reduction='sum')
        loss_dict = {"cls_loss": conf_loss}
        return loss_dict

    def get_regression_loss(self, predicted_locations, loc_targets, labels, priors=None):
        pos_mask = labels > 0  # ignore background
        pos_idx = pos_mask.unsqueeze(pos_mask.dim()).expand_as(predicted_locations)

        loc_p = predicted_locations[pos_idx].view(-1, 4)
        loc_t = loc_targets[pos_idx].view(-1, 4)

        if self.g_iou:
            g_iou_priors = priors.data.unsqueeze(0).expand_as(predicted_locations)
            loc_loss = self.g_iou(loc_p, loc_t, g_iou_priors[pos_idx].view(-1, 4))
        else:
            loc_loss = F.smooth_l1_loss(loc_p, loc_t, reduction='sum')

        loss_dict = {"reg_loss": loc_loss}
        return loss_dict
