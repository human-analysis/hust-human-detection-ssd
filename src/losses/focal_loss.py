from abc import ABC

import torch
import torch.nn.functional as F

from src.losses.multibox_loss import MultiBoxLoss


class FocalLoss(MultiBoxLoss, ABC):
    def __init__(self, num_classes, boxes_loss_cfg, neg_pos_ratio=3, gamma=2, alpha=0.25, use_gpu=True, *args,
                 **kwargs):
        super().__init__(num_classes, boxes_loss_cfg, neg_pos_ratio, use_gpu, *args, **kwargs)
        self.gamma = gamma
        self.alpha = alpha

    def get_conf_fn(self):
        return self.get_focal_loss

    def get_loc_fn(self):
        return self.get_regression_loss

    def get_focal_loss(self, confidence, labels):
        assert confidence.size(2) == self.num_classes
        ignores = labels < 0
        labels[ignores] = 0

        pos_cls = labels > -1
        mask = pos_cls.unsqueeze(2).expand_as(confidence)
        conf_p = confidence[mask].view(-1, confidence.size(2)).clone()
        p_t_log = -F.cross_entropy(conf_p, labels[pos_cls], reduction='sum')
        p_t = torch.exp(p_t_log)

        # This is focal loss presented in the paper eq(5)
        conf_loss = -self.alpha * ((1 - p_t) ** self.gamma * p_t_log)
        loss_dict = {"cls_loss": conf_loss}
        return loss_dict
