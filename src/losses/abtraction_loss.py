import torch.nn as nn


class AbstractionLoss(nn.Module):
    def __init__(self, num_classes, use_gpu, *args, **kwargs):
        super().__init__()
        self.num_classes = num_classes
        self.use_gpu = use_gpu

    def get_conf_fn(self):
        raise NotImplementedError

    def get_loc_fn(self):
        raise NotImplementedError

    def forward(self, confidence, predicted_locations, labels, loc_targets, priors=None):
        """
        :param confidence: [batch, n_anchors, num_classes]
        :param predicted_locations: [batch, n_anchors, 4]
        :param labels: [batch, n_anchors]
        :param loc_targets: [batch, n_anchors, 4]
        :return:
        """
        assert confidence.size(2) == self.num_classes
        ignores = labels < 0
        labels[ignores] = 0

        conf_loss_dict = self.get_conf_fn()(confidence, labels)
        loc_loss_dict = self.get_loc_fn()(predicted_locations, loc_targets, labels, priors)

        pos_mask = labels > 0
        num_pos = pos_mask.long().sum(1, keepdim=True)

        # It is caused by data augmentation when crop the images. The cropping can distort the boxes
        N = max(num_pos.data.sum(), 1)  # to avoid divide by 0.

        loss_dict = conf_loss_dict
        loss_dict.update(loc_loss_dict)

        loss_dict = {loss: value / N for loss, value in loss_dict.items()}
        return loss_dict
