- [What is hard negative mining?](https://www.quora.com/What-is-hard-negative-mining)
    * For reducing the false part or negative samples
- [Loss]()
    * [x] Multibox loss
    * [x] Focal loss
    * [x] Repulsion loss
       + Stills bugs(fixed size of matched prediction box)
       + TODO: doesn't fix inheritance for focal repulsion loss 
    * [ ] Additional boxes loss
       + Smooth f1(added)
       + Iou (Iou, Giou, Diou, Ciou)(added) still bugs (difference from box form -> fixing)
            + https://zhuanlan.zhihu.com/p/97555019
            + https://zhuanlan.zhihu.com/p/95010585
- [BackBone]()
    * [x] EfficientNet-b0 with (pretrained), not have advprop
    * [x] EffcientNet_Lite-b(0->4) with not pretrained(train from raw)
    * [x] MobileNet(support v2, v3 currently)
    * [how-is-mobilenet-v3-faster-than-v2](https://stackoverflow.com/questions/56949807/how-is-mobilenet-v3-faster-than-v2)
    * [Everything you need to know about MobileNetV3](https://towardsdatascience.com/everything-you-need-to-know-about-mobilenetv3-and-its-comparison-with-previous-versions-a5d5e5a6eeaa)
    * [ ] Check:
        Efficient-lite ssdlite, ssd better when focalloss + f1 smooth
        cIOU is the worst, 
        MobilenetV2 ssd ssdlite better with focallos + f1 smooth
        MobilenetV3 ssd quite sure with focallos + f1 smooth(ssdlite is not good, take a long time to reach 23, 7 map(37000 iter))
        Fully training for focalloss except mobilenet_v2_ssd(add later)
- [Result]()
    * Focal Loss (f1, Iou, Giou, Diou, CIou)
    * Mobilenetv2 - ssd():
        * f1()
        * CIou()
        * Iou()
        * Diou()
        * GIou()
    * Mobilenetv2 - ssdlite():
        * f1()
        * CIou()
        * Iou()
        * Diou()
        * GIou()
    * Efficientnet - ssdlite():
        * f1()
        * CIou()
        * Iou()
        * Diou()
        * GIou()
    * AdvpropEfficientnet - ssdlite():
        * f1()
        * CIou()
        * Iou()
        * Diou()
        * GIou()
    * Efficientnet - fssd():
        * f1()
        * CIou()
        * Iou()
        * Diou()
        * GIou()
    * AdvpropEfficientnet - fssd():
        * f1()
        * CIou()
        * Iou()
        * Diou()
        * GIou()
    * Efficientnet - fssdlite():
        * f1()
        * CIou()
        * Iou()
        * Diou()
        * GIou()
    * AdvpropEfficientnet - fssdlite():
        * f1()
        * CIou()
        * Iou()
        * Diou()
        * GIou()
- Tracker()
    * Part 1: Mobilenetv2 - ssd, fssd + (deep, sort, iou), (Adv)Effcientnet-b0 - ssdlite, 
                        fssdlite + (deep, sort, iou), Centernet(res_18_dcn, res_18) + (deep, sort, iou)(May be)(due date: 02/05)
    * Part 2: CenterNet + deepsort, YoloJDE(02/05 -> 15/05)
    * Part 3: Report(15/05 -> 31/05)