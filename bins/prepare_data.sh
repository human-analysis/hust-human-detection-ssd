# for training data
python3 -m src.engine.data_preparing --data_dir datasets/mot/train/ --mot_seq KITTI-17
python3 -m src.engine.data_preparing --data_dir datasets/mot/train/ --mot_seq MOT17-09-SDP
python3 -m src.engine.data_preparing --data_dir datasets/mot/train/ --mot_seq MOT17-11-SDP
python3 -m src.engine.data_preparing --data_dir datasets/mot/train/ --mot_seq MOT17-13-SDP
python3 -m src.engine.data_preparing --data_dir datasets/mot/train/ --mot_seq MOT20-01
python3 -m src.engine.data_preparing --data_dir datasets/mot/train/ --mot_seq MOT20-02
python3 -m src.engine.data_preparing --data_dir datasets/mot/train/ --mot_seq TUD-Campus
python3 -m src.engine.data_preparing --data_dir datasets/mot/train/ --mot_seq TUD-Stadtmitte


# for testing data
python3 -m src.engine.data_preparing --data_dir datasets/mot/test/ --mot_seq MOT17-04-SDP
python3 -m src.engine.data_preparing --data_dir datasets/mot/test/ --mot_seq PETS09-S2L1