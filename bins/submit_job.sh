ENTRY_PATH=$(readlink -f "$0")

printUsage() {
  echo "Usage: $(basename "$ENTRY_PATH") train | stop {cuda_id} {backbone} {detector} {loss}"
  echo
  echo " - train {cuda_id} {backbone} {detector} {loss}"
  echo " - stop: kill the program"
  echo " - cll: clean log files of the program"
}


#export log_root=/data/anhlt11/logs/efficientnet-detection/
export log_root=./logs/
case "$1" in
train)
  #  export CUDA_VISIBLE_DEVICES=$1
  export d=$2
  export backbone=$3
  export detector=$4
  export loss=$5
  export lower_loss=$5
  typeset -l lower_loss=$lower_loss
  export cfg_file=configs/${backbone}_b0_ssd${detector}300.yaml
  export log_path=${log_root}/${backbone}_b0_ssd${detector}300_${lower_loss}

  # shellcheck disable=SC2086
  nohup python3 train.py --config-file "${cfg_file}" --loss ${loss} &>${log_path} &
  #  done
  ;;

stop)
  # kill other worker
  # shellcheck disable=SC2016
  # shellcheck disable=SC2216
  ps aux | echo "${cfg_file}" | grep select | awk '{print "kill -9 " }' | bash
  ;;

retrain)
  $ENTRY_PATH stop $APP_PROF
  echo "Waiting..."
  sleep 5
  $ENTRY_PATH start $APP_PROF
  ;;

help)
  printUsage
  echo
  ;;

*)
  printUsage
  echo
  ;;

esac
