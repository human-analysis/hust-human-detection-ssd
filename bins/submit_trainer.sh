#export CUDA_VISIBLE_DEVICES=3; nohup python3 train.py --config-file configs/mobilenet_v3_small_ssd320.yaml --loss FocalLoss &> /data/anhlt11/logs/efficientnet-detection/mobilenet_v3_small_ssd320_focalloss &
#export CUDA_VISIBLE_DEVICES=3; nohup python3 train.py --config-file configs/mobilenet_v3_small_ssdlite320.yaml --loss FocalLoss &> /data/anhlt11/logs/efficientnet-detection/mobilenet_v3_small_ssdlite320_focalloss &
#export CUDA_VISIBLE_DEVICES=3; nohup python3 train.py --config-file configs/mobilenet_v2_ssdlite320.yaml --loss FocalLoss &> /data/anhlt11/logs/efficientnet-detection/mobilenet_v2_ssdlite320_focalloss &
#
#export CUDA_VISIBLE_DEVICES=2; nohup python3 train.py --config-file configs/mobilenet_v2_ssd320.yaml --loss FocalLoss &> /data/anhlt11/logs/efficientnet-detection/mobilenet_v2_ssd320_focalloss &
#
#export CUDA_VISIBLE_DEVICES=4; nohup python3 train.py --config-file configs/efficient_lite_b0_ssd300.yaml --loss FocalLoss &> /data/anhlt11/logs/efficientnet-detection/efficient_lite_b0_ssd300_focalloss &
#export CUDA_VISIBLE_DEVICES=4; nohup python3 train.py --config-file configs/efficient_lite_b0_ssdlite300.yaml --loss FocalLoss &> /data/anhlt11/logs/efficientnet-detection/efficient_lite_b0_ssdlite300_focalloss &
#
#export CUDA_VISIBLE_DEVICES=5; nohup python3 train.py --config-file configs/efficient_net_b0_ssd300.yaml --loss FocalLoss &> /data/anhlt11/logs/efficientnet-detection/efficient_net_b0_ssd300_focalloss &
##export CUDA_VISIBLE_DEVICES=5; nohup python3 train.py --config-file configs/efficient_net_b0_ssdlite300.yaml --loss FocalLoss &> /data/anhlt11/logs/efficientnet-detection/efficient_net_b0_ssdlite300_focalloss &
#
#export CUDA_VISIBLE_DEVICES=4; nohup python3 train.py --config-file configs/mobilenet_v2_ssd320.yaml --loss FocalLoss --boxes_loss Ciou &> /data/anhlt11/logs/efficientnet-detection/mobilenet_v2_ssd320_focalloss_ciou &
#export CUDA_VISIBLE_DEVICES=5; python3 train.py --config-file configs/efficient_net_b0_ssd300.yaml --loss FocalLoss

#export CUDA_VISIBLE_DEVICES=5; nohup python3 train.py --config-file configs/mobilenet_v2_ssd320.yaml --loss FocalLoss --boxes_loss SmoothL1 --num_core 4 &> /data/anhlt11/logs/efficientnet-detection/mobilenet_v2_ssd320_focalloss_smoothl1_box &
#export CUDA_VISIBLE_DEVICES=5; nohup python3 train.py --config-file configs/mobilenet_v2_ssd320.yaml --loss FocalLoss --boxes_loss Ciou --num_core 4 &> /data/anhlt11/logs/efficientnet-detection/mobilenet_v2_ssd320_focalloss_ciou_box &

#export CUDA_VISIBLE_DEVICES=4; nohup python3 train.py --config-file configs/mobilenet_v2_ssd320.yaml --loss FocalLoss --boxes_loss Giou --num_core 4 &> /data/anhlt11/logs/efficientnet-detection/mobilenet_v2_ssd320_focalloss_giou_box &
export CUDA_VISIBLE_DEVICES=4; nohup python3 train.py --config-file configs/mobilenet_v2_ssd320.yaml --loss FocalLoss --boxes_loss Diou --num_core 4 &> /data/anhlt11/logs/efficientnet-detection/mobilenet_v2_ssd320_focalloss_diou_box &
